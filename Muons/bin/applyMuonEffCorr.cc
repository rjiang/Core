#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>

#include "Core/Objects/interface/Lepton.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/GenericSFApplier.h"
#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TRegexp.h>

#include "Math/VectorUtil.h"

#include <darwin.h>

#include "Core/Muons/interface/toolbox.h"

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Muon {

////////////////////////////////////////////////////////////////////////////////
/// Adapted from [CMSSW](https://github.com/cms-sw/cmssw/blob/CMSSW_9_4_X/DataFormats/MuonReco/interface/Muon.h#L185-L212)
enum Selector {
    None                   = 0u, // added
    CutBasedIdLoose        = 1u<< 0,  
    CutBasedIdMedium       = 1u<< 1,  
    CutBasedIdMediumPrompt = 1u<< 2,  // medium with IP cuts
    CutBasedIdTight        = 1u<< 3,  
    CutBasedIdGlobalHighPt = 1u<< 4,  // high pt muon for Z',W' (better momentum resolution)
    CutBasedIdTrkHighPt    = 1u<< 5,  // high pt muon for boosted Z (better efficiency)
    PFIsoVeryLoose         = 1u<< 6,  // reliso<0.40
    PFIsoLoose             = 1u<< 7,  // reliso<0.25
    PFIsoMedium            = 1u<< 8,  // reliso<0.20
    PFIsoTight             = 1u<< 9,  // reliso<0.15
    PFIsoVeryTight         = 1u<<10,  // reliso<0.10
    TkIsoLoose             = 1u<<11,  // reliso<0.10
    TkIsoTight             = 1u<<12,  // reliso<0.05
    SoftCutBasedId         = 1u<<13,  
    SoftMvaId              = 1u<<14,  
    MvaLoose               = 1u<<15,  
    MvaMedium              = 1u<<16,  
    MvaTight               = 1u<<17,
    MiniIsoLoose           = 1u<<18,  // reliso<0.40
    MiniIsoMedium          = 1u<<19,  // reliso<0.20
    MiniIsoTight           = 1u<<20,  // reliso<0.10
    MiniIsoVeryTight       = 1u<<21   // reliso<0.05
};

////////////////////////////////////////////////////////////////////////////////
/// Guesses the selection from the histogram name in the maps, and applies it by
/// setting the weights to 0 to muons that don't pass the selection.
///
/// Note: tuned for Run 2 UL
class IDApplier : public GenericSFApplier<DAS::RecMuon> {
    const bool abseta, charge, IPCut;
    Selector num, den;
public:
    TString name, NUM, DEN;

private:

    ////////////////////////////////////////////////////////////////////////////////
    /// Translate table name from Muon POG ROOT file into selector.
    static Selector getMask (const string& s)
    {
        // RECO
        if (s == "TrackerMuons") return Selector::None;
        if (s == "genTracks"   ) return Selector::None;

        // Muon ID
        if (s ==        "LooseID") return Selector::CutBasedIdLoose       ;
        if (s ==       "MediumID") return Selector::CutBasedIdMedium      ;
        if (s == "MediumPromptID") return Selector::CutBasedIdMediumPrompt;
        if (s ==         "SoftID") return Selector::SoftCutBasedId        ;
        if (s ==        "TightID") return Selector::CutBasedIdTight       ; // andIPCut?
        if (s ==       "HighPtID") return Selector::CutBasedIdGlobalHighPt; // andIPCut?
        if (s ==    "TrkHighPtID") return Selector::CutBasedIdTrkHighPt   ; // andIPCut?

        // Muon ISO
        if (s == "VeryLooseRelIso") return Selector::PFIsoVeryLoose;
        if (s ==     "LooseRelIso") return Selector::PFIsoLoose    ;
        if (s ==    "MediumRelIso") return Selector::PFIsoMedium   ;
        if (s ==     "TightRelIso") return Selector::PFIsoTight    ;
        if (s == "VeryTightRelIso") return Selector::PFIsoVeryTight;
        if (s ==   "LooseRelTkIso") return Selector::TkIsoLoose    ;
        if (s ==   "TightRelTkIso") return Selector::TkIsoTight    ;

        BOOST_THROW_EXCEPTION( invalid_argument(s + " is not recognised.") );
    }

public:
    IDApplier (const fs::path& filePath, TString histname,
               bool correction, bool uncertainties)
        : GenericSFApplier(filePath, correction, uncertainties)
        , abseta(!histname.Contains("_eta_pt" )) // default is to take $|\eta|$
        , charge( histname.Contains("_charge" ))
        , IPCut ( histname.Contains("andIPCut"))
        , name  ( histname                     )
    {
        loadNominal(histname.Data());
        auto var = histname + "_stat";
        loadBinWiseUnc(var.Data(), var.Data(), UseBinError);
        var = histname + "_syst";
        loadGlobalUnc(var.Data(), var.Data(), UseBinError);

        histname.ReplaceAll("_eta_pt", "");
        histname.ReplaceAll("_abseta_pt", "");
        histname.ReplaceAll("_charge", "");
        histname.ReplaceAll("andIPCut", "");

        DEN = histname(TRegexp("_DEN_.*"));
        histname.ReplaceAll(DEN, "");
        DEN.ReplaceAll("_DEN_", "");
        den = getMask(DEN.Data());

        NUM = histname;
        NUM.ReplaceAll("NUM_", "");
        num = getMask(NUM.Data());

        cout << "Initialisation of " << __func__ << " done" << endl;
    }

protected:
    bool passes (const RecMuon& muon) const override
    {
        // TODO: IPCut? (unless it's already included in IPCut?)
        return num == 0 // Non standard, tracker SF
            || (num & muon.selectors) == num;
    }

    int binIndex (const RecMuon& muon,
                  const std::unique_ptr<TH1>& hist) const override
    {
        const float eta = abseta ? fabsf(muon.p4.Eta()) : muon.p4.Eta();
        if (charge) {
            // 3D: Q eta pT
            const float maxpt = hist->GetZaxis()->GetXmax() - 1;
            const float pt = min(muon.p4.Pt(), maxpt);
            return hist->FindBin(muon.Q, eta, pt);
        } else {
            // 2D: eta pT
            const float maxpt = hist->GetYaxis()->GetXmax() - 1;
            const float pt = min(muon.p4.Pt(), maxpt);
            return hist->FindBin(eta, pt);
        }
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Generic executable for all types of muon efficiency corrections.
/// The command should be called once for each table.
///
/// [TWiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/MuonReferenceEffsRun2)
/// [Tables](https://gitlab.cern.ch/cms-muonPOG/muonefficiencies)
///
/// ==== RECO
/// NUM = TrackerMuons
/// DEN = genTracks
/// - binning: abseta_pt
/// - uncertainties: -- (?)
///
/// ==== Muon ID
/// NUM = HighPtID LooseID MediumID MediumPromptID SoftID TightID TrkHighPtID   
/// DEN = TrackerMuons
/// - binning: abseta_pt
/// - uncertainties: stat, syst
///
/// ==== Muon ISO
///              DEN  \ NUM  LooseRelIso LooseRelTkIso TightRelIso TightRelTkIso
/// LooseID                      x                                            
/// MediumID                     x                       x
/// MediumPromptID               x                       x
/// TightID & IPCut              x                       x
/// HighPtID & IPCut                         x                       x
/// TrkHighPtID & IPCut                      x                       x
/// - binning: abseta_pt
/// - uncertainties: stat, syst
///
/// ==== trigger
///             DEN  \ NUM          IsoMu24 IsoMu24_or_Mu50 Mu50_or_OldMu100_or_TkMu100
/// IdMedium & PFIsoMedium            x            x
/// IdTight & PFIsoTight              x            x
/// IdGlobalHighPt & TkIsoLoose                                         x
/// - binning: abseta_pt, charge_abseta_pt, charge_eta_pt, eta_pt
/// - uncertainties: stat, syst
///
/// ==== tracking
/// Quoting the TWiki:
///
/// > Official tracking POG SFs, where the probe is a standalone
/// > muon and it is required to be matched to a generalTrack in its vicinity, are
/// > independent of momentum. The SFs are found to be above 0.99, and are considered
/// > equivalent to unity. You can find the detailed plots per eta and pT bins in this
/// > [TWiki](https://twiki.cern.ch/twiki/bin/view/CMSPublic/TrackingPOGResults2018).
/// > The recommendation are to not apply them, unless you target a precison analysis.
void applyMuonEffCorr 
       (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
        const fs::path& output, //!< output ROOT file (n-tuple)
        const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
        const int steering, //!< parameters obtained from explicit options 
        const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    unique_ptr<TChain> tIn = DT::GetChain(inputs);
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = unique_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);

    vector<RecMuon> * recMuons = nullptr;
    tIn->SetBranchAddress("recMuons", &recMuons);

    auto efficiency = config.get<string>("corrections.muons.efficiency");
    if (efficiency == "") {
        auto& efficiencies = config.get_child("corrections.muons.efficiency");
        if (efficiencies.size() == 0)
            BOOST_THROW_EXCEPTION( invalid_argument("Missing efficiency") );
        auto it = efficiencies.begin();
        while (metainfo.Find("corrections", "muons", "efficiency", it->first))
            ++it;
        efficiency = it->first;
    }
    auto [filename, histname] = getLocation(efficiency);
    cout << filename << ' ' << histname << endl;

    IDApplier applier(filename, histname, filename != "/dev/null", steering & DT::syst);
    if (metainfo.Find("corrections", "muons", "efficiency")) {
        // 0 = previous, former
        auto efficiency0 = metainfo.Get<string>("corrections", "muons", "efficiency");
        auto [filename0, histname0] = getLocation(efficiency0);
        if (filename == filename0 && filename != "/dev/null")
            cerr << orange << "The same file has been used twice in a row, "
                              "which is unexpected: `" << filename << "`. "
                              "Proceed at your own risks.\n" << def;
        IDApplier applier0(filename0, histname0, false, false);
        if (applier.DEN != applier0.NUM)
            cerr << orange << "The former and present muon efficiency corrections are "
                              "unexpected combinations: `" << applier0.name << "` and `"
                           << applier.name <<"`. Proceed at your own risks.\n" << def;
    }
    metainfo.Set<string>("corrections", "muons", "efficiency", efficiency);

    for (const auto& name: applier.weightNames())
        metainfo.Set<string>("variations", RecMuon::WeightVar, name);

    // TODO: ControlPlots
    
    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        applier(*recMuons);

        static const bool fillTree = (steering & DT::fill) == DT::fill;
        if (fillTree) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);
    fOut->cd();
    tOut->Write();

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of namespace DAS::Muon

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Apply muon scale factors (and corresponding selection) "
                    "for reconstruction, identification, and isolation. The command "
                    "should be called for each correction (a priori 3 times). "
                    "You may also run the selection only if you provide the histname.",
                            DT::config | DT::split | DT::fill | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<string>("efficiency", "corrections.muons.efficiency",
                            "`/path/to/file.root:histname` "
                            "(`/dev/null` for the filename to only apply selection)");
        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Muon::applyMuonEffCorr(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif

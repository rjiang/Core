class TF1;

namespace DAS::DoubleCrystalBall {

////////////////////////////////////////////////////////////////////////////////
/// Base functor for any Double Crystal Ball function
/// 
/// The idea is manyfold:
///  - store the values of the parameters and of the intermediate
///    steps of the calculation for potential debugging
///  - keep the same structure for the distribution itself
///    as well as for the integral of for the derivative
struct Base {

    double N, //!< global normalisation (best is to fix it to one)
           mu, //!< mean
           sigma, //!< width
           kL, //!< LHS turn-on point
           nL, //!< LHS power
           kR, //!< RHS turn-on point
           nR; //!< RHS power

    double AL, //!< LHS tail normalisation
           AR, //!< RHS tail normalisation
           BL, //!< LHS offset
           BR; //!< RHS offset

    double z, //!< normalised resolution
           aR, //!< normalised RHS turn-on point
           aL; //!< normalised LHS turn-on point

    double expaR2, //!< value of core at normalised RHS turn-on point
           expaL2; //!< value of core at normalised LHS turn-on point

    double CR, //!< area in RHS tail
           CL, //!< area in LHS tail
           DD, //!< area in core
           K;  //!< total normalisation

    double result; //!< store last evaludation

    ////////////////////////////////////////////////////////////////////////////////
    /// Calculate the normalised version of x for Gaussian core
    inline double Z (double x)
    {
        return (x-mu)/sigma;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Retrieves the parameters from p and calculate all intermediate steps of the 
    /// calculation.
    void SetParameters (double * p);

    ////////////////////////////////////////////////////////////////////////////////
    /// Virtual method to calculate the value of the function, given the internal set
    /// of parameters and the value of x given as parameter.
    virtual void Eval (double * x) = 0;

    ////////////////////////////////////////////////////////////////////////////////
    /// Standard form as requested from ROOT TF1 objects.
    double operator() (double * x, double *p);

    ////////////////////////////////////////////////////////////////////////////////
    /// If a TF1 is given, then the parameters are taken from f
    Base (TF1 * f = nullptr);

    ////////////////////////////////////////////////////////////////////////////////
    /// Just dump all internal parameters AND the last result
    void dump ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Default destructor will be taken from the daughter class
    virtual ~Base () = default;
};

////////////////////////////////////////////////////////////////////////////////
/// Functor to get the standard Double Crystal Ball distribution
/// 
/// TODO: write LaTeX version
/// 
/// *Note*: the formula looks a little bit different from the "Wikipedia" formula
/// to avoid too large numbers in the intermediate steps of the calculation.
struct Distribution : public Base {
    void Eval (double * x) final;
};

////////////////////////////////////////////////////////////////////////////////
/// Functor to get the integral of the Double Crystal Ball function
///
/// The integral is useful when cosntructing the response matrix.
/// 
/// TODO: write LaTeX version
struct Integral : public Base {
    void Eval (double * x) final;
};

////////////////////////////////////////////////////////////////////////////////
/// Functor to get the first derivative of the logarithm of the Double
/// Crystal Ball function
///
/// The reason for such a function is that the logarithm of a Gaussian is just
/// a parabola, and its derivative a simple line. This provides an easy way to
/// find the transition point.
/// 
/// TODO: write LaTeX version
struct DLog : public Base {
    void Eval (double * x) final;
};

} // end of DAS::DoubleCrystalBall namespace

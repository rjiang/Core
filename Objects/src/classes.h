#ifndef _DAS_CLASSES_
#define _DAS_CLASSES_

#include "Core/Objects/interface/Weight.h"
#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Lepton.h"
#include "Core/Objects/interface/Photon.h"
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Di.h"

DAS::Weight weight;
DAS::Weights weights;

DAS::GenJet genjet;
DAS::RecJet recjet;

DAS::GenMuon genmuon;
DAS::RecMuon recmuon;

DAS::GenPhoton genphoton;
DAS::RecPhoton recphoton;

DAS::GenEvent genevent;
DAS::RecEvent recevent;

DAS::Trigger dastrigger;
DAS::PileUp pileup;
DAS::MET met;
DAS::PrimaryVertex primaryvertex;

DAS::GenDijet gendijet;
DAS::GenDimuon gendimuon;
DAS::GenZJet genzjet;
DAS::RecDijet recdijet;
DAS::RecDimuon recdimuon;
DAS::RecZJet reczjet;

#endif

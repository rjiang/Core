#include "Core/Unfolding/interface/PtY.h"

#include "Math/VectorUtil.h"
#include <exceptions.h>

using namespace std;
using namespace DAS::Unfolding::InclusiveJet;
namespace DE = Darwin::Exceptions;

PtYFiller::PtYFiller (const PtY& obs, TTreeReader& reader)
    : obs(obs)
    , genJets(initOptionalBranch<decltype(genJets)>(reader, "genJets"))
    , recJets(reader, "recJets")
    , gEv(initOptionalBranch<decltype(gEv)>(reader, "genEvent"))
    , rEv(reader, "recEvent")
{
}

list<int> PtYFiller::fillRec (Variation& v)
{
    list<int> binIDs;

    double evW = rEv->Weight(v);
    if (obs.isMC) evW *= (*gEv)->Weight(v);

    for (const RecJet& recJet: recJets) {
        double pt = recJet.CorrPt(v),
               y  = recJet.AbsRap();
        if (!selection(pt,y)) continue;

        auto irecbin = obs.recBinning->GetGlobalBinNumber(pt,y);

        if (find(binIDs.begin(), binIDs.end(), irecbin) == binIDs.end())
            binIDs.push_back(irecbin);

        double jetW = recJet.Weight(v);
        v.tmp->Fill(irecbin, evW * jetW);
        v.rec->Fill(irecbin, evW * jetW);
    }
    return binIDs;
}

void PtYFiller::match ()
{
    matches.clear();
    misses.clear();
    fakes.clear();

    std::copy(genJets->begin(), genJets->end(), back_inserter(misses));
    std::copy(recJets.begin(), recJets.end(), back_inserter(fakes));
    for (auto genJet = misses.begin(); genJet != misses.end();) {
        bool matched = false;
        for (auto recJet = fakes .begin(); recJet != fakes .end(); ++recJet) {
            using ROOT::Math::VectorUtil::DeltaR;
            if (DeltaR(genJet->p4, recJet->p4) > obs.maxDR) continue;
            matched = true;
            matches.push_back({*genJet, *recJet});
            misses.erase(genJet);
            fakes .erase(recJet);
            break;
        }
        if (!matched)
            ++genJet;
    }
}

void PtYFiller::fillMC (Variation& v)
{
    if (!obs.isMC)
        BOOST_THROW_EXCEPTION( runtime_error(__func__ + " should only be called for MC"s) );

    double rEvW = rEv->Weight(v),
           gEvW = (*gEv)->Weight(v);

    for (const pair<GenJet,RecJet>& match: matches) {
        const auto& genJet = match.first;
        const auto& recJet = match.second;

        double genpt = genJet.CorrPt(v),
               geny  = genJet.AbsRap();

        bool goodGen = selection(genpt, geny);
        auto igenbin = obs.genBinning->GetGlobalBinNumber(genpt,geny);

        double gJetW = genJet.Weight(v);

        if (goodGen) v.gen->Fill(igenbin, gEvW * gJetW);

        double recpt = recJet.CorrPt(v),
               recy  = recJet.AbsRap();

        bool goodRec = selection(recpt, recy);
        auto irecbin = obs.recBinning->GetGlobalBinNumber(recpt,recy);
         
        double rJetW = recJet.Weight(v);

        if      ( goodRec &&  goodGen) { v.RM         ->Fill(igenbin, irecbin, gEvW * gJetW *      rEvW * rJetW );
                                         v.missNoMatch->Fill(igenbin,          gEvW * gJetW * (1 - rEvW * rJetW)); }
        else if (!goodRec &&  goodGen)   v.missOut    ->Fill(igenbin,          gEvW * gJetW                     );
        else if ( goodRec && !goodGen)   v.fakeOut    ->Fill(         irecbin, gEvW * gJetW *      rEvW * rJetW );
    }

    for (const GenJet& genJet: misses) { // TODO: use RM underflow?

        double pt = genJet.CorrPt(v),
               y  = genJet.AbsRap();
        if (!selection(pt,y)) continue;

        double gJetW = genJet.Weight(v);
        auto igenbin = obs.genBinning->GetGlobalBinNumber(pt,y);

        v.missNoMatch->Fill(igenbin, gEvW * gJetW);
        v.gen        ->Fill(igenbin, gEvW * gJetW);
    }

    for (const RecJet& recJet: fakes) {

        double pt = recJet.CorrPt(v),
               y  = recJet.AbsRap();
        if (!selection(pt,y)) continue;

        double rJetW = recJet.Weight(v);
        auto irecbin = obs.recBinning->GetGlobalBinNumber(pt,y);

        v.fakeNoMatch->Fill(irecbin, gEvW * rEvW * rJetW);
    }
}

////////////////////////////////////////////////////////////////////////////////

PtY::PtY () :
    Observable(__FUNCTION__, "Inclusive jet double differential cross section")
{
    recBinning->AddAxis("pt",nRecBins,recBins.data(),false,false);
    recBinning->AddAxis("y" ,  nYbins,y_edges.data(),false,false);
    genBinning->AddAxis("pt",nGenBins,genBins.data(),false,false);
    genBinning->AddAxis("y" ,  nYbins,y_edges.data(),false,false);
}

unique_ptr<DAS::Unfolding::Filler> PtY::getFiller (TTreeReader& reader) const
{
    return make_unique<PtYFiller>(*this, reader);
}

void PtY::setLmatrix (const unique_ptr<TH1>& bias, unique_ptr<TH2>& L)
{
    for (int iy = 1; iy <= nYbins; ++iy)
    for (int ipt = 1; ipt <= nGenBins; ++ipt) {

        double  y = (y_edges.at(iy -1) + y_edges.at(iy )) / 2,
               pt = (genBins.at(ipt-1) + genBins.at(ipt)) / 2;
        int i = genBinning->GetGlobalBinNumber(pt, y);
        if (i == 0)
            BOOST_THROW_EXCEPTION( runtime_error("pt = "s + pt + " and y = "s + y
                                        + " do not correspond to any bin index"s) );

        // b(in)
        int               bUp     = i-nGenBins,
             bLeft = i-1, bCenter = i         , bRight  = i+1,
                          bDown   = i+nGenBins;

        // values (curvature regularisation)
        auto get = [&bias](int i) {
            auto content = bias->GetBinContent(i);
            if (content < 0)
                BOOST_THROW_EXCEPTION( DE::BadInput("Expecting only positive entries", bias) );
            return content > 0 ? 1./content : 0;
        };

        auto cUp    = get(bUp   ),
             cLeft  = get(bLeft ),
             cRight = get(bRight),
             cDown  = get(bDown );

        cout << setw(3) << iy << setw(3) << ipt
             << setw(5) << bCenter << setw(15) << (cUp+cLeft+cRight+cDown)
             << setw(5) << bUp     << setw(15) << -cUp                   
             << setw(5) << bLeft   << setw(15) <<     -cLeft             
             << setw(5) << bRight  << setw(15) <<           -cRight      
             << setw(5) << bDown   << setw(15) <<                  -cDown << '\n';

        // filling L-matrix
                        L->SetBinContent(i, bCenter,  cUp+cLeft+cRight+cDown );
        if (cUp    > 0) L->SetBinContent(i, bUp    , -cUp                    );
        if (cLeft  > 0) L->SetBinContent(i, bLeft  ,     -cLeft              );
        if (cRight > 0) L->SetBinContent(i, bRight ,           -cRight       );
        if (cDown  > 0) L->SetBinContent(i, bDown  ,                  -cDown );
    }
    cout << flush;
}

#pragma once

#include <vector>
#include <list>
#include <optional>

#include <TUnfoldBinning.h>
#include <TTreeReaderArray.h>
#include <TH1.h>
#include <TH2.h>

#if !defined(__CLING__) || defined(__ROOTCLING__)
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Lepton.h"
#include "Core/Unfolding/interface/Observable.h"
#endif

namespace DAS::Unfolding::DrellYan {

static const std::vector<double> recPtBins{
        0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5,
        9, 9.5, 10, 10.5, 11, 11.5, 12, 12.5, 13, 13.5, 14, 15, 16, 17, 18, 19,
        20, 21, 22, 23, 25, 26, 28, 30, 32, 34, 37, 40, 43, 48, 52, 58, 65, 70,
        85, 102, 120, 140, 160, 175, 190, 205, 220, 235, 250, 275, 300, 350,
        400, 800, 1500},
    genPtBins{
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 18, 20, 22, 25,
        28, 32, 37, 43, 52, 65, 85, 120, 160, 190, 220, 250, 300, 400, 1500},
    yBins{0, 0.4, 0.8, 1.2, 1.6, 2.4};

static const int nRecPtBins = recPtBins.size()-1,
                 nGenPtBins = genPtBins.size()-1,
                 nYbins   = yBins.size()-1;

static const double minPt = 25;
static const double maxEta = 2.4;
static const double minMll = 76.1876, maxMll = 106.1876;

#if !defined(__CLING__) || defined(__ROOTCLING__)
////////////////////////////////////////////////////////////////////////////////
/// Following SMP-17-010 (https://doi.org/10.17182/hepdata.91215.v4)
struct ZPtY final : public Observable {

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    ZPtY ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructs a filler for the observable
    std::unique_ptr<Filler> getFiller (TTreeReader& reader) const override;

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Observable::setLmatrix`
    void setLmatrix (const std::unique_ptr<TH1>&, std::unique_ptr<TH2>&) override;
};

struct ZPtYFiller final : public Filler {
    ZPtY obs; ///< Backreference to the observable

    std::optional<TTreeReaderArray<GenMuon>> genMuons;
    TTreeReaderArray<RecMuon> recMuons;
    std::optional<TTreeReaderValue<GenEvent>> gEv;
    TTreeReaderValue<RecEvent> rEv;

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    ZPtYFiller (const ZPtY& obs, TTreeReader& reader);

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Filler::fillRec`
    std::list<int> fillRec (Variation&) override;

    ////////////////////////////////////////////////////////////////////////////////
    /// Matching is not needed.
    void match () override {}

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Filler::fillMC`
    void fillMC (Variation&) override;
};
#endif

} // end of DAS::Unfolding::DrellYan namespace

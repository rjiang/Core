#ifndef DAS_UNFOLDING_PTY
#define DAS_UNFOLDING_PTY

#include <vector>
#include <list>
#include <optional>

#include <TUnfoldBinning.h>
#include <TTreeReaderArray.h>
#include <TH1.h>
#include <TH2.h>

#if !defined(__CLING__) || defined(__ROOTCLING__)
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"
#include "Core/Unfolding/interface/Observable.h"
#endif

namespace DAS::Unfolding::InclusiveJet {

static const std::vector<double> recBins{/*74,84,*/97,114,133,153,174,196,220,245,272,300,330,362,395,430,468,507,548,592,638,686,737,790,846,905,967,1032,1101,1172,1248,1327,1410,1497,1588,1684,1784,1890,2000,2116,2238,2366,2500,2640,2787,2941,3103,3273,3450,3637,3832},
                                 genBins{/*74   ,*/97    ,133    ,174    ,220    ,272    ,330    ,395    ,468    ,548    ,638    ,737    ,846    ,967     ,1101     ,1248     ,1410     ,1588     ,1784     ,2000     ,2238     ,2500     ,2787     ,3103     ,3450,     3832},
                                 y_edges{0., 0.5, 1.0, 1.5, 2.0, 2.5, 3.0};

static const int nRecBins = recBins.size()-1,
                 nGenBins = genBins.size()-1,
                 nYbins   = y_edges.size()-1;

static const double minpt = recBins.front(), maxpt = recBins.back(), maxy = y_edges.back();

#if !defined(__CLING__) || defined(__ROOTCLING__)
struct PtY final : public Observable {
    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    PtY ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructs a filler for the observable
    std::unique_ptr<Filler> getFiller (TTreeReader& reader) const override;

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Observable::setLmatrix`
    void setLmatrix (const std::unique_ptr<TH1>&, std::unique_ptr<TH2>&) override;
};

struct PtYFiller final : public Filler {
    PtY obs; ///< Backreference to the observable

    std::optional<TTreeReaderArray<GenJet>> genJets;
    TTreeReaderArray<RecJet> recJets;
    std::optional<TTreeReaderValue<GenEvent>> gEv;
    TTreeReaderValue<RecEvent> rEv;

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    PtYFiller (const PtY& obs, TTreeReader& reader);

    ////////////////////////////////////////////////////////////////////////////////
    /// Jet selection
    inline bool selection (double pt, double absy)
    {
        return pt >= minpt && pt < maxpt && absy < maxy;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Filler::fillRec`
    std::list<int> fillRec (Variation&) override;

    std::vector<std::pair<GenJet,RecJet>> matches;
    std::vector<GenJet> misses;
    std::vector<RecJet> fakes;

    ////////////////////////////////////////////////////////////////////////////////
    /// Highest-pt matching, looping first on `GenJet`s.
    ///
    /// TODO: rather implement JERC matching?
    void match () override;

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Filler::fillMC`
    void fillMC (Variation&) override;
};
#endif

} // end of DAS::Unfolding::InclusiveJet namespace
#endif

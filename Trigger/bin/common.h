#include <fstream>
#include <iostream>

namespace DAS::Normalisation {

struct TriggerLumi {
    int pt, turnon;
    float weight;
    TriggerLumi (int Pt, int Turnon, float Weight) :
        pt(Pt), turnon(Turnon), weight(Weight)
    {}
    TriggerLumi (std::ifstream& infile)
    {
        infile >> pt;
        infile >> turnon;

        float lumi;
        infile >> lumi;
        weight = 1./lumi;
    }
};

std::ostream& operator<< (std::ostream& Stream, const TriggerLumi& tr_lu)
{
    Stream << tr_lu.pt << '\t' << tr_lu.turnon << '\t' << tr_lu.weight << '\t' << 1./tr_lu.weight;
    return Stream;
}

} // end of DAS::Normalisation namespace

//using DAS::Normalisation::operator<<;

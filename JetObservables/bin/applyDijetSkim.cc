#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Di.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TString.h>
#include <TFile.h>
#include <TH1.h>

#include "Math/VectorUtil.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Jets {

////////////////////////////////////////////////////////////////////////////////
/// Checks whether an event should be kept
template<class Jet>
bool keepEvent(const vector<Jet>& jets, float pt1, float pt2)
{
    return jets.size() >= 2 && jets[0].p4.Pt() > pt1 && jets[1].p4.Pt() > pt2;
}

////////////////////////////////////////////////////////////////////////////////
/// Skims the input tree to keep only events with at least two jets, with
/// configurable pT thresholds.
void applyDijetSkim
           (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
            const fs::path& output, //!< output ROOT file (n-tuple)
            const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
            const int steering, //!< parameters obtained from explicit options 
            const DT::Slice slice = {1,0} //!< number and index of slice
            )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    unique_ptr<TChain> tIn = DT::GetChain(inputs);
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = unique_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);

    const auto pt1 = config.get<float>("skims.dijet.pt1");
    metainfo.Set<float>("skims", "dijet", "pt1", pt1);

    const auto pt2 = config.get<float>("skims.dijet.pt2");
    metainfo.Set<float>("skims", "dijet", "pt2", pt2);

    cout << "Skimming events: pt(jet1) > " << pt1 << "\t pt(jet2) > " << pt2 << endl;

    vector<RecJet> * recJets = nullptr;
    RecDijet * recDijet = nullptr;
    if (branchExists(tIn, "recJets")) {
        tIn->SetBranchAddress("recJets", &recJets);
        recDijet = new RecDijet;
        tOut->Branch("recDijet", &recDijet);
    }

    vector<GenJet> * genJets = nullptr;
    GenDijet * genDijet = nullptr;
    if (branchExists(tIn, "genJets")) {
        tIn->SetBranchAddress("genJets", &genJets);
        genDijet = new GenDijet;
        tOut->Branch("genDijet", &genDijet);
    }

    if (!recJets && !genJets)
        BOOST_THROW_EXCEPTION( DE::BadInput("No jets in input tree", tIn) );

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        bool passesRec = true, passesGen = true;

        if (recJets) {
            passesRec = keepEvent(*recJets, pt1, pt2);
            if (passesRec) *recDijet = recJets->at(0) + recJets->at(1);
            else            recDijet->clear();
        }
        if (genJets) {
            passesGen = keepEvent(*genJets, pt1, pt2);
            if (passesGen) *genDijet = genJets->at(0) + genJets->at(1);
            else            genDijet->clear();
        }

        if ((steering & DT::fill) == DT::fill && (passesRec || passesGen)) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);
    tOut->Write();

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::Jets namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Selects events with at least two jets passing pT cuts "
                            "and creates dijet objects directly in the tree.",
                            DT::config | DT::split | DT::fill);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<float>("pt1", "skims.dijet.pt1", "Minimum pT of the first jet")
               .arg<float>("pt2", "skims.dijet.pt2", "Minimum pT of the second jet");
        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Jets::applyDijetSkim(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
